> _Not to be confused with [Klang the Incontinent Klingon](https://memory-beta.fandom.com/wiki/Klang_the_Incontinent_Klingon). For the compiler frontend for C-family languages, see [Clang](https://en.wikipedia.org/wiki/Clang). For the generation 5 Pokémon, see [Klang](https://en.wikipedia.org/wiki/List_of_generation_V_Pok%C3%A9mon#Klang)._

# 👋🏼 About Me

Hey, I'm Keelan (he/him)! I'm a **Senior Support Engineer** here at GitLab. I have a love for documentation and problem solving, so there's a high probability you've seen my name in docs merge requests and support tickets. You can check out my contributions section below if you interested to see what I've been working on!

I'm currently working on learning:
- [GitLab Geo](https://docs.gitlab.com/ee/administration/geo/#geo) administration, troubleshooting, and documenting (ongoing)
- PostgreSQL
- Macro photography

I have strong knowledge of:
- [SAML](https://docs.gitlab.com/ee/user/group/saml_sso/) and [SCIM](https://docs.gitlab.com/ee/user/group/saml_sso/scim_setup.html) topics
- Documentation and Technical Writing
- [GitLab Geo](https://docs.gitlab.com/ee/administration/geo/#geo)
- [GitLab Dedicated](https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/)

I'm also very proud of a few things at GitLab:
- [Pronoun Guidance and Information](https://about.gitlab.com/handbook/people-group/pronouns/)
- [All of these fun Merge Requests](https://gitlab.com/dashboard/merge_requests?scope=all&state=merged&author_username=klang)
- Ongoing participation in our [Pride TMRG](https://about.gitlab.com/company/culture/inclusion/tmrg-gitlab-pride/)

I'm a big proponent of the [ISO-8601 date format](https://www.iso.org/iso-8601-date-and-time-format.html), and I almost exclusively write dates using this format now. I find that it's much more likely to be (nearly) immediately understood by the largest number of readers, so I plan to stick with it!

## 🔍 Find me on

- [GitLab Team](https://about.gitlab.com/company/team/?department=customer-support#keelanlang)
- [LinkedIn](https://www.linkedin.com/in/keelanlang)

## 🕗 Availability

I'm located on the east coast in the United States, so I'm in the Eastern time zone. [Depending on the time of year](https://en.wikipedia.org/wiki/Eastern_Time_Zone#Daylight_saving_time), this is UTC-4 or UTC-5 (you can see what time it is for me [here](https://www.timeanddate.com/worldclock/@12212303)!). I don't have a well-defined working schedule, but if we need to schedule a sync meeting (for team members or customers!), my schedule is usually up to date. I typically am not online past 6PM Eastern.

## 🙋🏼 Feedback

I'm always working on improving myself, and one of the best ways for me to learn is to hear feedback from the people I work with. I believe that feedback is a gift, so if you'd like to share your input—positive or critical—please don't be afraid to let me know. I'm always looking for ways to improve, and so I always keep an open mind!

### As a GitLab team member

The best way to send feedback is by sending me a message on Slack. I may not respond right away, but I always make sure to read every message that comes my way. If you don't feel comfortable sharing feedback directly with me, you can also share it with my manager!

### As a GitLab customer

Since I almost exclusively work with customers through tickets, the best way to provide feedback is through the ticket feedback form. If the ticket is still open, you can find the feedback form in the signature of any of my responses. If the ticket is closed, our Support Bot will automatically send the feedback form 24 hours after closure.

In the event that you need to provide feedback outside the normal ticket process, you can reach me directly at the email above. Please note that I check my email infrequently, so it might take me some time to read and reply!

---

_Want a cool README for your own GitLab profile? Check out [add details to your profile with a README](https://docs.gitlab.com/ee/user/profile/#add-details-to-your-profile-with-a-readme)!_